# Challenge 1 mdef

Project by:

<a href="https://jana_tothillcalvo.gitlab.io/website/">Jana Tothill</a><br>
<a href="https://roger_guilemany.gitlab.io/mdef-website/">Roger Guilemany</a>

This is the documentation for the first challenge from fabacademy (mdef). Temporary name **Nomadic Challenge**.

The idea is to create a critical tool for designers to question their practices and reflect on them.

We want to play with the idea of mail art, going back to an analogig time. We feel we are overloading our screen time and it is time to go back to manual creativity and more intimate network. Run away from the digital immediace we have got used to, and enjoy more simple and slow connections.

The idea has been formalised as a box that gives you material (in form of text) to self-reflect on your practice as a designer. This box will be sent between designers, so their reflections don't only get stuck within them but shared with their close community. The first person that recieves the box will be challenged to add to it an object from the reflection he has made, and also add a tweet size note. When the person is done with the box, they will choose who is going to be the next one, and our selfs will pick it up and bring it to the next person, adding knowledge to the box in every iteration until it is full.

## References

**Mail Art**

This projectc gets inspired by the artistic practices that used the postal service as a way of experimentation and distibuition system of art during the sixties. The postal art movement, also called mail art or post art, got very influenced by the tradition of Fluxus, dadá, performative arts and site-specific practices. We want to bring back its spirit with this proposal, detach from digital media and embrace lo-tech practices.

*Contemporany references*

https://www.macba.cat/es/exposiciones-actividades/actividades/postdata

https://www.dear-data.com

https://www.moma.org/magazine/articles/309

<img src="images/references/mailart.png">

**S.M.S. (Shit Must Stop)**

http://sms.sensatejournal.com

Founded in New York City in 1968 by the artist, collector and dealer, William Copley, S.M.S (Shit Must Stop) was an art collection in a box, filled with small-scale, and often whimsical, artworks available by subscription. S.M.S editions were delivered to subscribers through the postal service, which offered Copley and his collaborator Dmitri Petrov a way to circumvent the art market and make contempoary art accesible to a wider audience.

Inspired by Copley’s mentor and friend, Marcel Duchamp, and his famous work Boîte-en-valise, S.M.S. was conceived as an interdisciplinary and intergenerational publication that would present the work of established and unknown artists without hierarchy. Regardless of their level of fame, each of the participants received $100 for their contribution.

S.M.S. brought together the works by 73 artists such as Bruce Conner, Richard Hamilton, Ray Johnson, On Kawara, Joseph Kosuth, Roy Lichtenstein, Marcel Duchamp, Lee Lozano, Bruce Nauman, Yoko Ono, Meret Oppenheim, Lil Picard, Man Ray, Terry Riley, Dieter Roth, Lawrence Weiner or La Monte Young, amongst others.

*(from https://bombonprojects.com/exhibition/s-m-s-shit-must-stop/)*

<img src="images/references/sms.png">

## Parts

For the formalisation of the concept we have had to define three main parts. The first one of those have been developing the main box, container of the information we are giving to the participants, as well as the objects these will put inside. Another part have been the graphics. The iterations here have been endless, but we have maneged to end with a final, easly replicable proposition. The final part is a small 'zine to cathalyse the reflections, according to Ron Wakkary's Nomadic Practices among the participants. We are also adding a set of labels in the box so the participants can write the name and adress of the next person that will recieve the box.

### Box

He have decided to go for a very simple box. The box is just the container, not the protagonist. At the same time we need to have a reliable and resistent box, as we don't know how it is going to be treated, and we need to move it arround the city.

As we were looking up to the S.M.S. project we wanted to follow a little bit their concept of carbord box, a rectangular one not very deep. We defined the dimentions to be **400x250x80**.

We started working on the design of the box by ourself, to later realise there are some online services that help you define your box at let you adjust the dimensions needed, ending with a defined template that we just had to adapt to be laser cutted.

This site really helped us on the box, and it have a lot of potential. It is interesting to keep it in mind for following projects where carboard boxes are needed.

https://easypackmaker.com/

We decided to go for the FEFCO 0427 model, as it was the most suited for our needs.

<img src="images/box/template-box.png">

https://easypackmaker.com/chooser/final?templateIndex=fefco_0427&ecma=No+filter&fefco=No+filter&envFoldBag=No+filter

#### Laser

The box whas cutted with laser at the MultiCam 2000 Laser machine in the Fab Lab Barcelona.

The used values have been:

*for cutting the edges*

- Power: 150
- Speed: 60.0

*for engraving the folds*

- Power: 50
- Speed: 100.0

<img src="images/box/laser-box.png">

<img src="images/box/final-box.png">

### Graphics

The graphics have been a long process, with a lot of iterations. The fist idea was to use silk-screen prinitng. The amount of text we had, made us look for alternatives to having to get the letters one by one from the vinyl. Also, because of the dimensions of the letters, we where afraid it would not paint to clear in the cardboard.

We decided to go for an stencil approach and spray painting, as we felt it would be easier if we could cut the template material in the laser cutter insted of the vinyl cutter. We have done different iterations with different materials.

#### Printing

##### Paper

We started working on the idea of having the tamplate for the stencil on a thick paper that we laser cut.

*for cutting the edges*

- Power: 40
- Speed: 5.0
- Hz: 1000

The paper resulted to be too thin, and after some tests, it started to get wet because of the paint. Also the results where not veary pleasant, as we where incapable to have it completly in contact with the surface we were spraying.

<img src="images/graphics/paper-graphics.png">

##### Cardboard

Looking for a more resistent material to the paint, that wouldn't get as wet, we try to use carboard. We had scraps from the box and the machine was already set up, so it was an easy solution worth trying.

In a similar way as the paper, the result were not very good. Because the carboard has to lauers separed by the corrugated, the paint would not come straight an dthe results where not very readable.

<img src="images/graphics/cardboard-graphics.png">

##### PETG

After two failures we found in the scraps materials a board of PETG. Its consistency and flexibility where interesting for us, as it would work quite good as a template.

After some test on the MultiCam 2000 Laser machine, to find the correct values we came up with the following:

*for cutting the edges*

- Power: 100
- Speed: 70.0

The problem with PETG is that it melts preatty easly, and the pieces cutted stuch in the material. That ment having to take them out one by one, not an easy task. Also the edges where melted and not straight. The results were a little frustrating, as we spent a lot of time setting and testing the machine, and it didn't work.

<img src="images/graphics/petg-graphics.png">

#### Stencil Tests

<img src="images/graphics/test-graphics-1.png">

<img src="images/graphics/test-graphics-2.png">

##### Vinyl

After all the test failing, we came back to the original idea of vinyl cutting. It resulted to, yes, be the most tirying one as we need to get the interiors of the letters one by one, but as the same time the most clean and readable.

We started prinitng the original idea of a lot of text, but the task was inmense and quite frustraitng. We simplify the vinilys and look for another way to present the information to the recievers of the box. The simplifyed graphics not only have helped in the process of making the box but also we are much more happ with the results

<img src="images/graphics/vinyl-graphics-1.png">

<img src="images/graphics/vinyl-graphics-2.png">

<img src="images/graphics/vinyl-graphics-3.png">

### Zine

As we discarded the idea of having all the information we wanted printed in the box we needed another format to share it. We came with an easy solution, chosing to create a small 'zine. This would serve two purposes, to make the reading and digestion of it much more easy, and to serve as a memory for the participant of the experience, as each box will have more than one of these.

The text of the 'zine is a collection of ideas from the Nomadic Practice paper by Ron Wakkary.

#### Nomadic Practices: A Posthuman Theory for Knowing Design

**Ron Wakkary**

This article develops the theory of nomadic practices as an alternative to seeing design as a humanist discipline. Nomadic practices is an epistemological theory guided by posthumanist commitments of phenomenological intentionality, situated knowledges, and nomadism. In contrast to humanist understandings of design that rely on objectivist viewpoints and universalizing foundations, nomadic practices see knowledge production in design as situated, embodied, and partial. The aim of the theory of nomadic practices is to remove the epistemological hurdles of a disciplinary structure such that design practices can be more expansive and plural. The article builds on prior epistemological theories including Kuhn’s (1962) paradigms, Redström’s (2017) programs, and Agre’s (1997) generative metaphor as seen through past changes and upheavals in what is considered design, such as Bødker’s (2006) third wave HCI (human-computer interaction) or Harrison et al.’s (2007) paradigms of HCI. It then turns to key posthumanist concepts to articulate structural features of nomadic practices, namely 1) multiplicity of intentionalities; 2) situated knowing; and 3) nomadism. The contribution of this article is to offer a theory for thinking about design that embraces multiplicity and diversity rather than universalizing and singular ways of knowing design.

http://www.ijdesign.org/index.php/IJDesign/article/view/4039/928

#### Text of the Zine

**Introduction**

Consider the following ...
Physicalise these thoughts on an prototype/practice and put it in the box.
Add a tweet size reflection of the experience

--

From what perspectives are you talking about design?
What are you trying to unbuild?
Reflect on the multiplicity of intentionalities of your practice.
Is it seeking plurality? How?
Why?
Is knowledge being expansive or inclusionary rather than exclusionary?
What is your objectivist viewpoint?
Is your position unbiased or neutral? it doesn’t have to be, but WHY?
Does it have a conflicting foundation?

--

designers are  human and non-human
designers create biographies
constituencies  create  designers
humans and non-humans co-create worlds
things are inherently  political
politics is a matter of more than human participation.

--

human-things are interconnected
human-things are transformative
human-things are relational
human-things are share agency

--

cyborg manifesto
humans and non-humans share centre stage
design for more-than-humans

--

Multiplicity
Situated Knowing
Nomadism

<img src="images/zine/zine-1.png">

## Final

Next you can see some images of the final result.

We have already contacted Saúl Baeza and Luís Eslava, designers from Barcelona, that will recive one box each to start circulating them. We are expecting to see how the project evolves, we see it as a powerful tool that can result in inspiring and unexpected objects and reflections.

<img src="images/final/final-box-1.png">

<img src="images/final/final-box-2.png">

<img src="images/final/final-box-3.png">

<img src="images/final/final-box-4.png">

## Future

For the moment we have focused on the development and formalisation of the project. Now that it will start moving and gathering knowledge, we will start to think in the after. What are we doing once it is full? Different options emerge, such as doing a collective reflection with all the participants or preparing a small exhibition with the objects and reflections of each person. Maybe it can go to a digital format and become a website where we post the content of each box...


